package Actions;

import Book.*;
import Database.*;
import UI.*;

import java.util.List;


public class UserActions {

    //switch with actions
    public Integer selectAction(Integer numb, String[] values) {
        IWorkWithUsersInConsole ui = new WorkWithUsersInConsole();
        switch (numb) {
            case 1:
                return AddBook(values);
            case 2:
                return ui.SelectInf(SelectBookFromISBN(values[0]));
            case 3:
                return ui.WriteInfAboutBook(FindFromKey(values));
            case 4:
                return CheckUser(values[0], values[1]);
            case 5:
                return AddUser(values[0], values[1]);
            default:
                return -1;
        }
    }

    public IBook SelectBookFromISBN(String ISBN) { //null or Book

        IDatabase database = new Database();
        int num = database.OpenConnection();
        if (num != 0)
            return null;
        IBook book = database.Read(ISBN);
        num = database.CloseConnection();
        if (num != 0)
            return null;
        return book;
    }

    public List<Book> FindFromKey(String[] keys) {
        IDatabase database = new Database();
        int num = database.OpenConnection();
        if (num != 0)
            return null;
        List<Book> books = database.Read(keys);
        if (books.size() == 0)
            return null;
        num = database.CloseConnection();
        if (num != 0)
            return null;
        return books;
    }

    //check exist user in database
    public Integer CheckUser(String login, String password) {
        return WorkWithDatabase(2, new String[]{login, password});
    }

    public Integer AddUser(String login, String password) {
        return WorkWithDatabase(1, new String[]{login, password});
    }

    public Integer AddBook(String[] values) { //0 or -1 or -2
        return WorkWithDatabase(3, values);
    }

    public Integer WorkWithDatabase(int numb, String[] value) {
        IDatabase database = new Database();
        int num = database.OpenConnection();
        int res;
        if (num != 0)
            return num;
        switch (numb) {
            case 1:
                res = database.WriteUser(value[0], value[1]);
                break;
            case 2:
                res = database.CheckUser(value[0], value[1]);
                break;
            case 3:
                res = database.WriteBook(value[0], value[1], value[2], value[3], value[4], value[5]);
                break;
            default:
                return -20;
        }
        if (res != 0)
            return res;
        num = database.CloseConnection();
        if (num != 0)
            return num;
        return res;

    }


}
