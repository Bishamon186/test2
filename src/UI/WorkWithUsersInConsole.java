package UI;

import Book.Book;
import Database.Database;
import Database.IDatabase;
import org.springframework.security.crypto.bcrypt.BCrypt;
import Actions.UserActions;
import Book.IBook;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class WorkWithUsersInConsole implements IWorkWithUsersInConsole {

    private String login = null;
    private String password = null;
    private String loginWithoutCrypt = null;


    //select info about book
    public Integer SelectInf(IBook book) {

        Scanner in = new Scanner(System.in);

        if(book==null){
            System.out.println("Book not found");
            return 0;
        }

        while (true) {
            System.out.println("So, what do you know?\n" +
                    "1 - title of the book\n" +
                    "2 - author's name\n" +
                    "3 - annotation\n" +
                    "4 - date of publication\n" +
                    "5 - website\n" +
                    "6 - back");
            String numb = in.nextLine();
            return WriteInfoAboutBook(numb, book);
        }
    }

    //write info about book
    public Integer WriteInfoAboutBook(String numb, IBook book) {
        if (book == null){
            System.out.println("Book not found");
            return -1;
        }

        switch (numb) {
            case "1":
                System.out.println(book.getTitle());
                break;
            case "2":
                System.out.println(book.getName());
                break;
            case "3":
                System.out.println(book.getAnnotation());
                break;
            case "4":
                System.out.println(book.getDate());
                break;
            case "5":
                System.out.println(String.format("https://www.labirint.ru/search/%s/", book.getTitle().replace(" ", "%20")));
                break;
            case "6":
                return 0;
        }
        return 0;
    }


    //login user
    public Integer Login() {

        Scanner in = new Scanner(System.in);

        if (login == null || password == null) {
            System.out.println("You are a new user?\n1-yes, I am a new user\n2-no");
            String numb = in.nextLine();
            System.out.println("Input login: ");
            String Mylogin = in.nextLine();
            System.out.println("Input password: ");
            String Mypassword = in.nextLine();
            return LoginInf(numb, Mylogin, Mypassword);

        }
        return 0;
    }

    //take info about user for login
    public Integer LoginInf(String numb, String myLogin, String myPassword) {
        UserActions action = new UserActions();
        IDatabase db = new Database();
        if (numb.equals("2")) {
db.OpenConnection();
            if (db.CheckUser(myLogin,myPassword)== 0) {
                login = ((Database) db).login;
                password = ((Database) db).password;
                loginWithoutCrypt = myLogin;
                db.CloseConnection();
                return 0;
            } else {
                System.out.println("Incorrect login or password");
                db.CloseConnection();
                return -1;
            }

        } else if (numb.equals("1")) {
            login = BCrypt.hashpw(myLogin, BCrypt.gensalt(10));
            password = BCrypt.hashpw(myPassword, BCrypt.gensalt());
loginWithoutCrypt = myLogin;
            db.OpenConnection();
            if (db.CheckUser(myLogin,myPassword) == -1) {
                action.AddUser(login,password);
                System.out.println("You have been successfully registred");
                db.CloseConnection();
                return 0;
            } else {
                System.out.println("This login is already registered");
                login=null;
                password=null;
                db.CloseConnection();
                return -2;
            }

        } else return -1;
    }


    //take info about book from user
    public String[] TakeInfAboutBook() { // add user's id
        Scanner in = new Scanner(System.in);


        if (Login() != 0) {
            return null;
        }


        System.out.println("Enter title of the book: ");
        String title = in.nextLine();
        System.out.println("Enter author's name: ");
        String name = in.nextLine();
        System.out.println("Enter annotation: ");
        String annotation = in.nextLine();
        System.out.println("Enter ISBN: ");
        String ISBN = in.nextLine();
        System.out.println("Enter date of publication: ");
        String date = in.nextLine();
        String[] res = new String[]{ISBN,title , name, annotation, date, loginWithoutCrypt};
        return res;
    }

//take keywords
    public String[] Keys() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter keywords separated by commas");
        String res = in.nextLine();
        return res.split(",");
    }

//take what user want
    public Integer Action() {
        Scanner in = new Scanner(System.in);
        Integer numb;

        System.out.println("1 - add the book\n" +
                "2 - select information about book by ISBN\n" +
                "3 - find the book by keywords\n" +
                "4 - information about program\n" +
                "5 - exit");
        try {
            numb = Integer.parseInt(in.nextLine());
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return -1;
        }


        UserActions action = new UserActions();
        switch (numb) {
            case 1:
                String[] s = TakeInfAboutBook();
                if(s ==null){
                    return -1;
                }
                if (action.selectAction(1, s) == -1) {
                    System.out.println("Book already exist");

                }

                break;
            case 2:
                System.out.println("Input ISBN:");
                action.selectAction(2, new String[]{in.nextLine()});
                break;
            case 3:
                action.selectAction(3, Keys());
                break;
            case 4:
                System.out.println("*******************************************************************************\n" +
                        "This console app - book database. If you want to add a book, you need to log in\n" +
                        "Also authorized users can save the book to favorites. Unauthorized users\n" +
                        "can only receive information about the book by ISBN or keywords. If the found\n" +
                        "keyword is in the annotation - you will see an alert.\n" +
                        "*******************************************************************************\n" +
                        "Technologists: Java, PostgreSQL.\n" +
                        "*******************************************************************************\n" +
                        "Created by Nastya Kulikova (abpopa12@mail.ru)");
                break;
            case 5:
                System.out.println("Bye!");
                return 5;

            default:
                System.out.println("Incorrect command");
                return -1;

        }
        return 0;
    }

    //write info about book (without annotation )
    public Integer WriteInfAboutBook(List<Book> books) {
        if (books == null || books.size() == 0){
            System.out.println("Book not found");
            return -1;
        }

        Collections.sort(books, Book.COMPARE_BY_COUNT);
        for (var book : books) {
            System.out.println(String.format("Title: %s", book.getTitle()));
            System.out.println(String.format("Date: %s", book.getDate()));
            System.out.println(String.format("Author's name: %s", book.getName()));
            System.out.println(String.format("ISBN: %s", book.getISBN()));
            if(book.IsAnnotation()){
                System.out.println(String.format("Keyword in the annotation: %s", book.getISBN()));
            }
        }
        return 0;
    }


}







