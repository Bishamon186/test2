package UI;

import Book.*;

import java.util.List;

public interface IWorkWithUsersInConsole {

    Integer SelectInf(IBook book);
    String[] TakeInfAboutBook();
    String[] Keys();
    Integer Login();
    Integer Action();
    Integer WriteInfAboutBook(List<Book> books );
    Integer WriteInfoAboutBook(String numb, IBook book);
    Integer LoginInf(String numb,String myLogin,String myPassword);

}
