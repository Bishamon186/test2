package Database;

import Book.Book;
import Book.IBook;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Database implements IDatabase {
    private Connection connection = null;

    public String login;
    public String password;

    //open database connection
    public int OpenConnection() {

        //  Database credentials
        final String DB_URL = "jdbc:postgresql://localhost:5432/books";
        final String USER = "meow";
        final String PASS = "0009";


        System.out.println("Connection to PostgreSQL JDBC");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return -1;
        }

        System.out.println("PostgreSQL JDBC Driver successfully connected");


        try {
            connection = DriverManager
                    .getConnection(DB_URL, USER, PASS);
            connection.setAutoCommit(false);

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
            return -2;
        }

        if (connection != null) {
            System.out.println("You successfully connected to database now");
            return 0;

        }
        return 0;
    }

    //write user to database
    public Integer WriteUser(String login, String password) {
        if (!IsExistUser(login)) {
            Write(String.format("INSERT INTO users (login,password) "
                    + "VALUES ('%s','%s');", login, password));
            return 0;
        } else
            return -1;

    }

    //write book to database
    public Integer WriteBook(String ISBN, String annotation, String title, String name, String date, String users) {
        if (!IsExistBook(ISBN)) {
            if (IsExistUser(users) ) {
                Write(String.format("INSERT INTO books (ISBN, annotation, title,name,date,users) " +
                        "VALUES ('%s','%s','%s','%s','%s','%s');", ISBN, annotation, title, name, date,login));
                return 0;
            } else
                return -2;
        } else
            return -1;

    }

    //write something to database
    private void Write(String str) {

        Statement stmt = null;
        try {

            stmt = connection.createStatement();
            String sql = str;
            stmt.executeUpdate(sql);
            stmt.close();
            connection.commit();

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Records created successfully");
    }

    //close database connection
    public int CloseConnection() {
        try {
            connection.close();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    //read one book from database
    public IBook Read(String ISBN) {
        Statement stmt = null;
        Book book;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM books WHERE ISBN = '%s';", ISBN));
            if (rs.next()) {
                book = new Book(rs.getString("title"), rs.getString("name"), rs.getString("annotation"), ISBN, rs.getString("date"));
                rs.close();
                stmt.close();
            } else {
                rs.close();
                stmt.close();
                return null;
            }


        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
        System.out.println("Operation done successfully");
        return book;
    }

    //read list book from database
    public List<Book> Read(String[] keyWords) {
        List<Book> books = new ArrayList<>();
        Statement stmt = null;
        Book book;

        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM books");
            while (rs.next()) {
                book = new Book(rs.getString("title"), rs.getString("name"), rs.getString("annotation"), rs.getString("ISBN"), rs.getString("date"));
                int count = 0;
                boolean isAnnotation = false;
                for (var word : keyWords) {
                    if (book.getAnnotation().contains(word)) {
                        count++;
                        isAnnotation = true;
                    }
                    if (book.getDate().contains(word))
                        count++;

                    if (String.valueOf(book.getISBN()).contains(word))
                        count++;
                    if (book.getName().contains(word))
                        count++;
                    if (book.getTitle().contains(word))
                        count++;
                }
                if (count != 0) {
                    books.add(new Book(rs.getString("title"), rs.getString("name"), rs.getString("annotation"), rs.getString("ISBN"), rs.getString("date"), isAnnotation, count));
                }
            }
            rs.close();
            stmt.close();

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
        System.out.println("Operation done successfully");
        return books;
    }

    //check exist login and password in database
    public Integer CheckUser(String login, String password) {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM users;");

            boolean check = false;

            while(rs.next()){
                String s = rs.getString("login");
                if(BCrypt.checkpw(login,rs.getString("login"))){
                    if(BCrypt.checkpw(password,rs.getString("password"))){
                        this.login = rs.getString("login");
                        this.password = rs.getString("password");
                        return 0;

                    }
                }
            }

            return -1;


        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return -3;
        }

    }

    //check exist something
    public Boolean IsExist(String query) {
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                rs.close();
                stmt.close();
                return true;
            } else {

                rs.close();
                stmt.close();
                return false;
            }


        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }

    //check exist user in database
    private Boolean IsExistUser(String login) {

        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM users;");

            boolean check = false;

            while(rs.next()){
                String s = rs.getString("login");
                if(BCrypt.checkpw(login,rs.getString("login"))){
                        this.login = rs.getString("login");
                        this.password = rs.getString("password");
                        return true;
                }
            }

            return false;


        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return false;
        }
    }

    //check exist book in database
    private Boolean IsExistBook(String ISBN) {
        return IsExist(String.format("SELECT * FROM books WHERE ISBN = '%s';", ISBN));
    }
}

