package Database;

import Book.*;

import java.util.List;

public interface IDatabase {

    int OpenConnection();

    Integer WriteUser(String login, String password);

    Integer WriteBook(String ISBN, String annotation, String title, String name, String date, String users);

    int CloseConnection();

    IBook Read(String ISBN);

    List<Book> Read(String[] keyWords);

    Integer CheckUser(String login, String password);

    Boolean IsExist(String query);
}
