package Tests;

import Actions.UserActions;
import Book.*;
import Book.IBook;
import Database.Database;
import Database.IDatabase;
import UI.WorkWithUsersInConsole;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.ArrayList;
import java.util.List;

public class Tests {
private String l = "nastya";
        private String p = "1234";
    private String annotaion = "Одна из величайших книг ХХ века.\n" +
            "Странная, поэтичная, причудливая история города Макондо, затерянного где-то в джунглях, - от сотворения до упадка. \n" +
            "История рода Буэндиа - семьи, в которой чудеса столь повседневны, что на них даже не обращают внимания. \n" +
            "Клан Буэндиа порождает святых и грешников, революционеров, героев и предателей, лихих авантюристов - и женщин, слишком прекрасных для обычной жизни. \n" +
            "В нем кипят необычайные страсти - и происходят невероятные события. \n" +
            "Однако эти невероятные события снова и снова становятся своеобразным \"волшебным зеркалом\", сквозь которое читателю является подлинная история Латинской Америки\n";
    private String title = "100 лет одиночества";
    private String annotation2 = "Своеобразный антипод второй великой антиутопии ХХ века - О дивный новый мир Олдоса Хаксли. Что, в сущности," +
            "страшнее: доведение до абсурда общество потребления - или доведение до абсолюта общество идеи?";
    private String name = "Габриэль Гарсиа Марсес";
    private String isbn = "9781856953214";
    private String date = "1967";
    private String name2 = "Джордж Оруэлл";
    private String isbn2 = "9780706427493";
    private String date2 = "1948";
    private String title2 = "1984";
    private Book firstBook;
    private Book secondBook;
    private IBook fourthBook;
    private WorkWithUsersInConsole ui = new WorkWithUsersInConsole();
    private UserActions action = new UserActions();
    private Book thirdBook;
    private IDatabase database =  new Database();
    private  List<Book> books;

    public void TestCreateBook() {
        firstBook = new Book(title, name, annotaion, isbn, date, true, 1);
        secondBook = new Book("1984", "Джордж Оруэлл", annotation2, "9780706427493", "1948", true, 2);
        thirdBook = new Book("100 лет одиночества", "Габриэль Гарсиа Марсес", annotaion, "9781856953214", "1967", true, 3);

    }
    public void TestGetIsAnnotationFromBookClass() {
        if (!firstBook.IsAnnotation()) {
            ExitTest("BookTest", "IsAnnotation");
        }
    }

    public void TestGetAnnotatinFromBookClass() {
        if (!firstBook.getAnnotation().equals(annotaion)) {
            ExitTest("BookTest", "getAnnotation");
        }
    }

    public void TestGetDateFromBookClass() {
        if (!firstBook.getDate().equals("1967")) {
            ExitTest("BookTest", "getDate()");
        }
    }

    public void TestGetISBNFromBookClass() {
        if (!firstBook.getISBN().equals("9781856953214")) {
            ExitTest("BookTest", "getISBN");
        }
    }

    public void TestGetCountKeyWordsFromBookClass(){
        if (firstBook.getCountKeyWords() != 1) {
            ExitTest("BookTest", "getCountKeyWords");
        }
    }

    public void TestSortBookByKeywordMatch(){

        List<Book> books = new ArrayList<Book>();
        books.add(firstBook);
        books.add(secondBook);
        books.add(thirdBook);

        if (books.get(0).getCountKeyWords() != 3 && books.get(0).getCountKeyWords() != 2 && books.get(0).getCountKeyWords() != 1) {
            ExitTest("BookTest", "Comparator");
        }

    }

    public void TestConnectionToDatabaseWithBooks() {
        if (database.OpenConnection() != 0) {
            ExitTest("DataBaseTest", "OpenConnection");
        }
    }

    public void TestWriteBookToDatabaseWithBooks() {
        if (database.WriteBook(isbn, annotaion, title, name, date, l) != 0) {
            ExitTest("DataBaseTest", "WriteBook");
        }
    }

    public void TestWriteBookWithExistISBNToDatabaseWithBooks() {
        if (database.WriteBook(isbn, annotaion, title, name, date, "sdsds") != -1) {
            ExitTest("DataBaseTest", "WriteBook");
        }
    }

    public void TestWriteUserToDatabaseWithBooks() {
        if (database.WriteUser(BCrypt.hashpw(l, BCrypt.gensalt(10)), BCrypt.hashpw(p, BCrypt.gensalt(10))) != 0) {
            ExitTest("DataBaseTest", "WriteUser");
        }
    }

    public void TestWriteExistUserToDatabaseWithBooks() {
        if (database.WriteBook("234234", annotaion, title, name, date, "nastfya") != -2) {
            ExitTest("DataBaseTest", "WriteBook");
        }
    }


    public void TestReadBookFromDatabase() {
        fourthBook = database.Read(isbn);
        if (fourthBook == null) {
            ExitTest("DataBaseTest", "ReadBook");
        }
    }

    public void TestGetTitleFromBookFromDatabase() {
        if (!fourthBook.getTitle().equals(title)) {
            ExitTest("DataBaseTest", "ReadBook");
        }
    }

    public void TestExistenceBookByKeyWordFromDatabase() {

        books = database.Read(new String[]{"Буэндиа"});
        if (books == null) {
            ExitTest("DataBaseTest", "ReadListBook");
        }
    }

    public void TestCountKeyWordsInTheBookFromListOfBooks() {
        if (books.get(0).getCountKeyWords() != 1) {
            ExitTest("DataBaseTest", "ReadListBook");
        }
    }
    public void TestTitleInTheBookFromListOfBooks() {
        if (!books.get(0).getTitle().equals(title)) {
            ExitTest("DataBaseTest", "ReadListBook");
        }
    }
    public void TestIsAnnotationInTheBookFromListOfBooks() {
        if (!books.get(0).IsAnnotation()) {
            ExitTest("DataBaseTest", "ReadListBook");
        }
    }

    public void TestNonexistentBookFromListOfBooksFromDatabase() {
        books = database.Read(new String[]{"sddc"});
        if (books.size() != 0) {
            ExitTest("DataBaseTest", "ReadListBook");
        }
    }

    public void CheckExistUserInDatabase() {
        if (database.CheckUser(l, p) != 0) {
            ExitTest("DataBaseTest", "CheckUser");
        }
        if (database.CheckUser("nastyda", "1234") != -1) {
            ExitTest("DataBaseTest", "CheckUser");
        }
    }
    public void CheckExistBookOnISBNInDatabase() {

        if (!database.IsExist(String.format("SELECT * FROM books WHERE users = '%s';", ((Database) database).login))) {
            ExitTest("DataBaseTest", "IsExist");
        }
        if (database.IsExist("SELECT * FROM books WHERE users = 'nastyfa';")) {
            ExitTest("DataBaseTest", "IsExist");
        }
    }

    public void CheckCloseConnectionDataBase(){
        if (database.CloseConnection() != 0) {
            ExitTest("DataBaseTest", "CloseConnection");
        }

    }

    public void TestWriteUserInDatabaseInActionsClass() {

        if (action.WorkWithDatabase(20, new String[]{}) != -20)
            ExitTest("ActionsTest", "WorkWithDatabase");
        if (action.WorkWithDatabase(1, new String[]{BCrypt.hashpw("meow", BCrypt.gensalt(10)), BCrypt.hashpw(p, BCrypt.gensalt(10))}) != 0)
            ExitTest("ActionsTest", "WorkWithDatabase");
    }

    public void TestCheckExistUserInDatabase(){
        if (action.WorkWithDatabase(2, new String[]{"meow", p}) != 0)
            ExitTest("ActionsTest", "WorkWithDatabase");
    }

    public void TestWriteBookInDatabaseInActionsClass() {
        if (action.WorkWithDatabase(3, new String[]{isbn2, annotation2, title2, name2, date2, l}) != 0)
            ExitTest("ActionsTest", "WorkWithDatabase");
    }
    public void TestSelectBookOnISBNInActionsClass() {
        if (action.SelectBookFromISBN(isbn2) == null)
            ExitTest("ActionsTest", "SelectBookFromISBN");
        if (!action.SelectBookFromISBN(isbn2).getTitle().equals("1984"))
            ExitTest("ActionsTest", "SelectBookFromISBN");
        if (action.SelectBookFromISBN("asxasx") != null)
            ExitTest("ActionsTest", "SelectBookFromISBN");
    }

    public void TestSelectBookUsingKeywordsInActionsClass(){
        if (!action.FindFromKey(new String[]{"1984"}).get(0).getTitle().equals("1984"))
            ExitTest("ActionsTest", "FindFromKey");
        if (action.FindFromKey(new String[]{"sdcsd"}) != null)
            ExitTest("ActionsTest", "FindFromKey");
        if (action.selectAction(20, new String[]{}) != -1)
            ExitTest("ActionsTest", "FindFromKey");

    }

    public void WorkWithUserClassTest() {

        if(ui.WriteInfoAboutBook("5", firstBook)!=0)
            ExitTest("UITest", "WriteInfoAboutBook");
        if(ui.WriteInfoAboutBook("5",null)!=-1)
            ExitTest("UITest", "WriteInfoAboutBook");

        if(ui.LoginInf("2","nastya2","1234")!=-1){
            ExitTest("UITest", "LoginInf");
        }
        if(ui.LoginInf("1","nastya2","1234")!=0){
            ExitTest("UITest", "LoginInf");
        }
        if(ui.LoginInf("2","nastya2","1234")!=0){
            ExitTest("UITest", "LoginInf");
        }
        if(ui.LoginInf("1","nastya2","1234")!=-2){
            ExitTest("UITest", "LoginInf");
        }
        if(ui.LoginInf("2","nastya2","123as4")!=-1){
            ExitTest("UITest", "LoginInf");
        }
        Success("UITest");
    }

    private void ExitTest(String erMess, String info) {
        System.out.println("Error in " + erMess);
        System.out.println("Error in " + info);
        System.exit(-1);
    }

    private void Success(String info) {
        System.out.println("Success " + info);
    }
}
