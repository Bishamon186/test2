package Book;

import java.util.Comparator;

public interface IBook {

    String getTitle();

    String getAnnotation();

    String getName();

    String getDate();

    String getISBN();

    boolean IsAnnotation();

    int getCountKeyWords();



}
