package Book;


import java.util.Comparator;

public class Book implements IBook {

    private String Title;
    private String Name;
    private String Annotation;
    private String ISBN;
    private String Date;
    private boolean IsAnnotation;
    private int CountKeyWords;


    public Book(String title, String name, String annotation, String isbn, String date) {
        Title = title;
        Name = name;
        Annotation = annotation;
        ISBN = isbn;
        Date = date;
    }

    public Book(String title, String name, String annotation, String isbn, String date, boolean isAnnotation, int countKeyWords) {
        Title = title;
        Name = name;
        Annotation = annotation;
        ISBN = isbn;
        Date = date;
        IsAnnotation = isAnnotation;
        CountKeyWords = countKeyWords;
    }

    public String getTitle() {
        return Title;
    }

    public String getAnnotation() {
        return Annotation;
    }

    public String getName() {
        return Name;
    }

    public String getDate() {
        return Date;
    }

    public String getISBN() {
        return ISBN;
    }

    public boolean IsAnnotation() {
        return IsAnnotation;
    }

    public int getCountKeyWords() {
        return CountKeyWords;
    }

    public static final Comparator<Book> COMPARE_BY_COUNT = new Comparator<Book>() {
        @Override
        public int compare(Book b1, Book b2) {
            return b1.getCountKeyWords() - b2.getCountKeyWords();
        }
    };
}
